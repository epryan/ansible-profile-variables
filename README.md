# ansible-profile-variables

## Getting started

```
ansible-playbook -i hosts main.yml -e @vars.yml
```

```
ansible-playbook -i hosts main.yml -e "profile_variables_record_vars='var_on_play:var_on_block:var_set_during_play'" -e "profile_variables_record_hosts='hosta'"

```

## Example Run

```
❯ ansible-playbook -i hosts main.yml -e @vars.yml

PLAY [all] ****************************
Parsing 'record_tasks' from string to list ...
record_tasks=[]
Using 'record_vars' list as is ...
record_vars=['var_on_play', 'var_on_block', 'var_set_during_play']
Using 'record_hosts' list as is ...
record_hosts=['hosta']

TASK [Debug 1] ****************************
ok: [hosta] => {
    "msg": "hello play"
}
ok: [hostb] => {
    "msg": "hello play"
}

TASK [Instantiate another value] ****************************
ok: [hosta]
ok: [hostb]

TASK [Debug 2] ****************************
ok: [hosta] => (item=var_on_play) => {
    "ansible_loop_var": "item",
    "item": "var_on_play",
    "var_on_play": "hello play"
}
ok: [hosta] => (item=var_set_during_play) => {
    "ansible_loop_var": "item",
    "item": "var_set_during_play",
    "var_set_during_play": "hello set_fact"
}
ok: [hostb] => (item=var_on_play) => {
    "ansible_loop_var": "item",
    "item": "var_on_play",
    "var_on_play": "hello play"
}
ok: [hostb] => (item=var_set_during_play) => {
    "ansible_loop_var": "item",
    "item": "var_set_during_play",
    "var_set_during_play": "hello set_fact"
}

TASK [Include Role] ****************************

TASK [OneDebugRole : Debug In Role] ****************************
ok: [hosta] => {
    "msg": "Consider it included!"
}
ok: [hostb] => {
    "msg": "Consider it included!"
}

TASK [Trigger Handler] ****************************
changed: [hosta] => {
    "msg": "Triggering Handler"
}
changed: [hostb] => {
    "msg": "Triggering Handler"
}

RUNNING HANDLER [handleit] ****************************
ok: [hosta] => {
    "msg": "Consider it handled"
}
ok: [hostb] => {
    "msg": "Consider it handled"
}

PLAY RECAP ****************************
hosta                      : ok=6    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
hostb                      : ok=6    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   

[
    {
        "host": "hosta",
        "task": "Debug 1",
        "task_arguments": {
            "msg": "{{ var_on_play }}"
        },
        "task_variables": {
            "var_on_block": "hello block"
        },
        "tracked_variables": {
            "var_on_play": "hello play",
            "var_on_block": "hello block",
            "var_set_during_play": "VARIABLE IS UNDEFINED"
        }
    },
    {
        "host": "hosta",
        "task": "Instantiate another value",
        "task_arguments": {
            "var_set_during_play": "hello set_fact"
        },
        "task_variables": {
            "var_on_block": "hello block"
        },
        "tracked_variables": {
            "var_on_play": "hello play",
            "var_on_block": "hello block",
            "var_set_during_play": "VARIABLE IS UNDEFINED"
        }
    },
    {
        "host": "hosta",
        "task": "Debug 2",
        "task_arguments": {
            "var": "{{ item }}"
        },
        "task_variables": {
            "var_on_block": "hello block"
        },
        "tracked_variables": {
            "var_on_play": "hello play",
            "var_on_block": "hello block",
            "var_set_during_play": "hello set_fact"
        }
    },
    {
        "host": "hosta",
        "task": "Include Role",
        "task_arguments": {
            "name": "OneDebugRole"
        },
        "task_variables": {
            "var_on_block": "hello block",
            "message": "Consider it included!"
        },
        "tracked_variables": {
            "var_on_play": "hello play",
            "var_on_block": "hello block",
            "var_set_during_play": "hello set_fact"
        }
    },
    {
        "host": "hosta",
        "task": "OneDebugRole : Debug In Role",
        "task_arguments": {
            "msg": "{{ message }}"
        },
        "task_variables": {
            "var_on_block": "hello block",
            "message": "Consider it included!"
        },
        "tracked_variables": {
            "var_on_play": "hello play",
            "var_on_block": "hello block",
            "var_set_during_play": "hello set_fact"
        }
    },
    {
        "host": "hosta",
        "task": "Trigger Handler",
        "task_arguments": {
            "msg": "Triggering Handler"
        },
        "task_variables": {
            "var_on_block": "hello block"
        },
        "tracked_variables": {
            "var_on_play": "hello play",
            "var_on_block": "hello block",
            "var_set_during_play": "hello set_fact"
        }
    },
    {
        "host": "hosta",
        "task": "handleit",
        "task_arguments": {
            "msg": "{{ message }}"
        },
        "task_variables": {
            "message": "Consider it handled"
        },
        "tracked_variables": {
            "var_on_play": "hello play",
            "var_on_block": "VARIABLE IS UNDEFINED",
            "var_set_during_play": "hello set_fact"
        }
    }
]
```

## Authors and acknowledgment
Max Mitschke, https://github.com/demonpig
Ryan Erickson, https://gitlab.com/epryan

## License
GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

## Project status
Proof of Concept